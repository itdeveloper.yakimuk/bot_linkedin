# ENTER YOUR TEXT HERE
prepared_text = 'test'


# EXCEPTION IF OUR BOT IS UNABLE TO LIST
class NotEnoughItemsForListError(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return 'NotEnoughItemsForListError: {}'.format(self.message)
        else:
            return 'NotEnoughItemsForListError has been raised'


# MAKE A LIST WITH NAMES ON THE PAGE
def make_list_of_names(spans):
    list_of_names = []
    for i in spans:
        counter = 0
        s = str(i.text)
        splits = s.split()
        if len(splits) > 1:
            for i in splits:
                if (i.isalpha() and i.title()) or (i.title() and '.' in i) or (i.title() and '-' in i):
                    counter += 1
                    if counter == len(splits):
                        list_of_names.append(splits[0])
    if len(list_of_names) > 9:
        return list_of_names
    else:
        raise NotEnoughItemsForListError('not enough items for list, check the function syntax')


# IT'S FOR CONNECT EVERY PEOPLE WITHOUT FILTER
def action(x):
    driver.get(
        f'https://www.linkedin.com/search/results/people/?geoUrn=%5B%22105072130%22%5D&network=%5B%22S%22%5D&origin=FACETED_SEARCH&page={x}&sid=Hoc')
    time.sleep(2)
    all_buttons = driver.find_elements_by_tag_name("button")
    connect_buttons = [btn for btn in all_buttons if
                       btn.text == "Connect" or btn.text == "Pending" or btn.text == "Follow"]
    for btn in connect_buttons:
        driver.execute_script("arguments[0].click();", btn)
        time.sleep(2)
        send = driver.find_element_by_xpath("//button[@aria-label='Send now']")
        driver.execute_script("arguments[0].click();", send)
    time.sleep(2)
    x = x + 1
    action(x)
